#define _DEFAULT_SOURCE
#include "test.h"

#include <inttypes.h>
#include <stdio.h>
#include <unistd.h>


#include "mem.h"
#include "mem_internals.h"
#include "util.h"

static void* heap;

static struct block_header* start_block;

void init_heap_mem(){
    heap = heap_init(10000);
    if (heap == NULL) {
        err ("Error in heap initialization\n");
    } else {
        debug_heap (stderr, heap);
    }
    start_block = (struct block_header*) heap;
}

void allocate_memory() {
    debug("--- Allocate memory start ---\n");
    void* test1_mem = _malloc(1000);
    if (test1_mem == NULL) {
        err ("Memory allocation failed!\n");
    }
    debug_heap (stderr, heap);
    if (start_block->capacity.bytes != 1000 || start_block->is_free) {
        err ("Memory allocation failed!\n");
    }
    debug ("Allocate memory end\n");
}

void free_one_block() {
    debug("--- One block free start ---\n");
    void* test2_mem = _malloc(1000);
    _malloc(1000);
    _free(test2_mem);
    debug_heap(stderr, heap);
    if (!((struct block_header*) (((uint8_t*) test2_mem)-offsetof(struct block_header, contents)))->is_free) {
        err("One block free failed!\n");
    }
    debug ("One block free end");
}

void free_two_blocks(){
    debug("--- Two blocks free start ---\n");
    void* test3_mem1 = _malloc(1000);
    void* test3_mem2 = _malloc(1000);
    _free(test3_mem1);
    _free(test3_mem2);
    debug_heap(stderr, heap);
    if (!((struct block_header*) (((uint8_t*) test3_mem1)-offsetof(struct block_header, contents)))->is_free ||
            !((struct block_header*) (((uint8_t*) test3_mem2)-offsetof(struct block_header, contents)))->is_free) {
        err("Two blocks free failed!\n");
    }
    debug ("Two blocks free end\n");
}

static void free_heap() {
    struct block_header* curr = start_block;
    while (curr != NULL) {
        if (!curr->is_free) {
            _free((void*) (((uint8_t*) curr) + offsetof(struct block_header, contents)));
            curr = curr->next;
        }
    }
}

void extends_old_region() {
    debug ("--- Region extends old region start ---\n");

    free_heap();

    void* test4_mem = _malloc(20000);

    debug_heap(stderr, heap);

    if (((struct block_header*) (((uint8_t*) test4_mem)-offsetof(struct block_header, contents))) != heap ||
            ((struct block_header*) (((uint8_t*) test4_mem)-offsetof(struct block_header, contents)))->is_free ||
            ((struct block_header*) (((uint8_t*) test4_mem)-offsetof(struct block_header, contents)))->capacity.bytes != 20000) {
        err ("Region extends old region failed!\n");
    }
    debug("Region extends old region end\n");

}

struct block_header* last_block() {
    for (struct block_header* last = start_block; ; last = last->next) {
        if (last->next == NULL) {
            return last;
        }
    }
}

void new_region_another() {
    debug ("--- New region another start ---\n");

    struct block_header* last = last_block();

    void* test5_taken_mem_addr = last + size_from_capacity(last->capacity).bytes;

    void* test5_taken_mem = mmap( (uint8_t*) (getpagesize() * ((size_t) test5_taken_mem_addr / getpagesize()
            + (((size_t) test5_taken_mem_addr % getpagesize()) > 0))),1000, PROT_READ | PROT_WRITE,
            MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);

    debug("Test taken memory address: %p\n", test5_taken_mem);

    void* test5_mem = _malloc(40000);

    debug_heap(stderr, heap);

    if (test5_mem == start_block->next || test5_mem == test5_taken_mem ||
        ((struct block_header*) (((uint8_t*) test5_mem)-offsetof(struct block_header, contents)))->capacity.bytes != 40000 ||
                ((struct block_header*) (((uint8_t*) test5_mem)-offsetof(struct block_header, contents)))->is_free) {
        err("New region another failed!\n");
    }
    debug ("New region another end\n");
    
}
