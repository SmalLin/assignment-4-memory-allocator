#ifndef _TEST_H_
#define _TEST_H_

void init_heap_mem();
void allocate_memory();
void free_one_block();
void free_two_blocks();
void extends_old_region();
void new_region_another();

#endif
