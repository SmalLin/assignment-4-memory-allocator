#include <stdio.h>
#include "test.h"

int main() {
    init_heap_mem();
    allocate_memory();
    free_one_block();
    free_two_blocks();
    extends_old_region();
    new_region_another();
    printf("---- Tests end ----\n");
}

